# Meddesign Stencil PoC

Test av komponentbibliotek byggt med [Stenciljs](https://stenciljs.com/)

Följande komponenter ska byggas

* Inputfält
* Combobox

Vi har jobbat innan i stencil med Digi men kanske inte på djupen som jag fick göra nu. Vi ville ha med den avskalade versionen utan monorepo och flera myndigheter.

### Förutsättningar

* [Nodejs](https://nodejs.org/en)

### Börja jobba

```js
  npm install
  npm run build
  npm start
```
  
### Bedömning

#### Intryck

Stencil är i grund och botten byggt på **Web Components** som är en webstandard för att skapa egna html element. Med kraften tillagt med JSX/TSX så blir det en bra kombination och lättare och roligare att jobba med en att bygga **Web Componets** från grunden.

Med Stencil så känns allt möjligt med att bygga avancerade komponenter och även att applicera olika sorters styling. Om vi nu vill använda oss av Design Tokens med css/scss eller om vi vill använda oss av tailwind

#### Underhåll

[Ionic](https://ionicframework.com/) står bakom stenciljs och den uppdateras frekvent och släpper nya versioner [ofta](https://www.npmjs.com/package/@stencil/core?activeTab=versions).

### Resurser / Dokumentation

Även om det är en ganska sparsmakad [dokumentation](https://stenciljs.com/docs/introduction) så finns ett community där användare får lägga upp nyttiga plugins. Även en [blogg](https://ionic.io/blog/tag/stencil?_gl=1*1r8i4jz*_ga*OTU5MzQyNjQwLjE3MDYwOTgwMTU.*_ga_REH9TJF6KF*MTcxNTk0MzMwNS4xMDQuMS4xNzE1OTQ2NzkyLjAuMC4w) som pratar om de senaste releaserna och vad som är på gång. En [discord](https://chat.stenciljs.com/?_gl=1*m2tspy*_ga*OTU5MzQyNjQwLjE3MDYwOTgwMTU.*_ga_REH9TJF6KF*MTcxNTk0MzMwNS4xMDQuMS4xNzE1OTQ2OTA5LjAuMC4w) kanal där man kan få hjälp.
