import { Component, EventEmitter, Prop, Event, State, Method, h } from '@stencil/core';

@Component({
  tag: 'mdd-form-input',
  styleUrl: 'form-input.scss',
})
export class MeddesignInput {
  private inputElement?: HTMLInputElement;

  @Prop() mddType = 'text';
  @Prop() mddVariation = '';
  @Prop() mddLabel = '';
  @Prop() mddHelpText = '';
  @Prop() mddValidationText = '';

  @State() _touched = false;
  @State() _focused = false;

  @Event() mddOnChange: EventEmitter;
  @Event() mddOnBlur: EventEmitter;
  @Event() mddOnKeyup: EventEmitter;
  @Event() mddOnFocus: EventEmitter;
  @Event() mddOnFocusout: EventEmitter;
  @Event() mddOnInput: EventEmitter;
  @Event() mddOnTouched: EventEmitter;

  @Method()
  async clear() {
    if (this.inputElement) {
      this.inputElement.value = '';
    }
  }

  onBlurHandler(e) {
    if (!this._touched) {
      this.mddOnTouched.emit(e);
      this._touched = true;
    }
    this.mddOnBlur.emit(e);
  }

  onChangeHandler(e) {
    this.mddOnChange.emit(e);
  }

  onFocusHandler(e) {
    this._focused = true;
    this.mddOnFocus.emit(e);
  }

  onFocusoutHandler(e) {
    this._focused = false;
    this.mddOnFocusout.emit(e);
  }

  handleInput(e) {
    this.mddOnInput.emit(e);
  }

  get cssModifiers() {
    return {
      'mdd-form-input--success': this.mddVariation === 'success',
      'mdd-form-input--error': this.mddVariation === 'error',
      'mdd-form-input--mddOnFocus': this._focused,
    };
  }

  render() {
    return (
      <div
        class={{
          'mdd-form-input': true,
          ...this.cssModifiers,
        }}
      >
        {this.mddLabel && <label>{this.mddLabel}</label>}
        {this.mddHelpText && <small>{this.mddHelpText}</small>}
        <div class="mdd-form-input__wrapper">
          <input
            ref={el => this.inputElement = el as HTMLInputElement}
            class="mdd-form-input__wrapper__input"
            onBlur={e => this.onBlurHandler(e)}
            onChange={e => this.onChangeHandler(e)}
            onFocus={e => this.onFocusHandler(e)}
            onFocusout={e => this.onFocusoutHandler(e)}
            onInput={e => this.handleInput(e)}
            type={this.mddType}
          />
          <svg
            class="mdd-form-input__wrapper__icon"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          >
            <path d="M1 12S5 4 12 4s11 8 11 8-4 8-11 8S1 12 1 12z" />
            <circle cx="12" cy="12" r="3" />
          </svg>
        </div>
        {this.mddValidationText && <small class="mdd-form-input__validation-text">{this.mddValidationText}</small>}
      </div>
    );
  }
}
