# mdd-form-input



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description | Type     | Default  |
| ------------------- | --------------------- | ----------- | -------- | -------- |
| `mddHelpText`       | `mdd-help-text`       |             | `string` | `''`     |
| `mddLabel`          | `mdd-label`           |             | `string` | `''`     |
| `mddType`           | `mdd-type`            |             | `string` | `'text'` |
| `mddValidationText` | `mdd-validation-text` |             | `string` | `''`     |
| `mddVariation`      | `mdd-variation`       |             | `string` | `''`     |


## Events

| Event           | Description | Type               |
| --------------- | ----------- | ------------------ |
| `mddOnBlur`     |             | `CustomEvent<any>` |
| `mddOnChange`   |             | `CustomEvent<any>` |
| `mddOnFocus`    |             | `CustomEvent<any>` |
| `mddOnFocusout` |             | `CustomEvent<any>` |
| `mddOnInput`    |             | `CustomEvent<any>` |
| `mddOnKeyup`    |             | `CustomEvent<any>` |
| `mddOnTouched`  |             | `CustomEvent<any>` |


## Methods

### `clear() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [mdd-form-select-filter](../form-select-filter)

### Graph
```mermaid
graph TD;
  mdd-form-select-filter --> mdd-form-input
  style mdd-form-input fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
