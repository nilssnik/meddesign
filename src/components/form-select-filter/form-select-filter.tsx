import { Component, Prop, State, h } from '@stencil/core';

@Component({
  tag: 'mdd-form-select-filter',
  styleUrl: 'form-select-filter.scss',
})
export class MeddesignFormSelectFilter {
  @Prop({ mutable: true }) mddButtonText = 'Välj land i listan';
  @Prop() mddList = [
    { id: 1, name: 'Sweden' },
    { id: 2, name: 'Finland' },
    { id: 3, name: 'Norway' },
    { id: 4, name: 'Denmark' },
    { id: 5, name: 'Austria' },
    { id: 6, name: 'Germany' },
    { id: 7, name: 'France' },
  ];
  @State() _filteredList = [...this.mddList];
  @State() _selectedItem = null;
  @State() _isOpen = false;
  @State() _focusedIndex = -1;

  private inputRef?: HTMLMddFormInputElement;

  toggleIsOpen = async () => {
    if (this._isOpen) {
      if (this.inputRef) {
        await this.inputRef.clear();
      }
      this._filteredList = this.mddList;
    }
    this._isOpen = !this._isOpen;
  };

  handleInputChange = (e: any) => {
    const searchWord = e.target.value.toLowerCase();
    if (!searchWord) {
      this._filteredList = this.mddList;
    } else {
      this._filteredList = this.mddList.filter(l => l.name.toLowerCase().includes(searchWord));
    }
    this._focusedIndex = -1;
  };

  handleKeyDown = (e: KeyboardEvent) => {
   
    switch (e.key) {
      case 'Enter':
      case ' ':
        if (this._isOpen && this._focusedIndex >= 0) {
          this.handleItemClick(this._filteredList[this._focusedIndex].id);
        } else {
          this.toggleIsOpen();
        }
        e.preventDefault();
        break;
      case 'ArrowDown':
        if (this._isOpen) {
          this._focusedIndex = (this._focusedIndex + 1) % this._filteredList.length;
          this.scrollToItem();
        } else {
          this._isOpen = true;
        }
        e.preventDefault();
        break;
      case 'ArrowUp':
        if (this._isOpen) {
          this._focusedIndex = (this._focusedIndex - 1 + this._filteredList.length) % this._filteredList.length;
          this.scrollToItem();
        } else {
          this._isOpen = true;
        }
        e.preventDefault();
        break;
      case 'Escape':
        this._isOpen = false;
        break;
      default:
        break;
    }
  };

  handleItemClick = (id: number) => {
    this._selectedItem = this.mddList.find(l => l.id === id);
    this._isOpen = false;
    this.mddButtonText = this._selectedItem.name;
  };

  scrollToItem() {
    const itemList = document.querySelectorAll('.search-result__item');
    if (this._focusedIndex >= 0 && itemList.length > this._focusedIndex) {
      const focusedItem = itemList[this._focusedIndex] as HTMLElement;
      focusedItem.scrollIntoView({ block: 'nearest', behavior: 'smooth' });
    }
  }

  get css_IsOpened() {
    return {
      'form-select-filter__dropdown-menu--open': this._isOpen,
    };
  }

  get cssHasSelectedValueModifier() {
    return {
      "form-select-filter__button-wrapper__button": true,
      "form-select-filter__button-wrapper__button--selected": this._selectedItem,
    }
  }

  render() {
    return (
      <div class="form-select-filter">
        <label>Välj land</label>
        <div class={{'form-select-filter__button-wrapper': true, 'form-select-filter__button-wrapper--open': this._isOpen}}>
          {this._selectedItem &&
            <svg class="form-select-filter__button-wrapper__icon form-select-filter__button-wrapper__icon--start" width="23" height="18" viewBox="0 0 23 18" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
              <path d="M20.547.18L7.315 13.627 2.453 8.686a.597.597 0 00-.853 0L.177 10.132a.62.62 0 000 .868l6.711 6.82c.236.24.618.24.854 0L22.823 2.493a.62.62 0 000-.867L21.4.18a.597.597 0 00-.853 0z" fill="green" stroke="green" fill-rule="nonzero" />
            </svg>
          }
          <button class={{ ...this.cssHasSelectedValueModifier }} tabIndex={0} onKeyDown={this.handleKeyDown} onClick={this.toggleIsOpen}>
            {this.mddButtonText}
          </button>
          <svg class="form-select-filter__button-wrapper__icon form-select-filter__button-wrapper__icon--end" width="14" height="8" viewBox="0 0 14 8" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"><path d="M1 1l6.074 6L13 1.147" stroke="currentColor" stroke-width="2" fill="none" fill-rule="evenodd"></path></svg>
        </div>
        <div class={{ 'form-select-filter__dropdown-menu': true, ...this.css_IsOpened }}>
          <div class="form-select-filter__content">
            <mdd-form-input ref={(el) => this.inputRef = el as HTMLMddFormInputElement} onKeyDown={this.handleKeyDown} onInput={this.handleInputChange} md-type="text" md-label="Sök land"></mdd-form-input>
            <ul class="search-result">
              {this._filteredList.map((item, i) => {
                const itemClass = {
                  'search-result__item': true,
                  'search-result__item--selected': this._selectedItem && this._selectedItem.id === item.id,
                  'search-result__item--focused': i === this._focusedIndex,
                };
                return (
                  <li class={itemClass} onClick={() => this.handleItemClick(item.id)} key={item.id} tabIndex={-1}>
                    {item.name}
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
