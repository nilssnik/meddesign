# mdd-form-select-filter



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute         | Description | Type                              | Default                                                                                                                                                                                                                              |
| --------------- | ----------------- | ----------- | --------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `mddButtonText` | `mdd-button-text` |             | `string`                          | `'Välj land i listan'`                                                                                                                                                                                                               |
| `mddList`       | --                |             | `{ id: number; name: string; }[]` | `[     { id: 1, name: 'Sweden' },     { id: 2, name: 'Finland' },     { id: 3, name: 'Norway' },     { id: 4, name: 'Denmark' },     { id: 5, name: 'Austria' },     { id: 6, name: 'Germany' },     { id: 7, name: 'France' },   ]` |


## Dependencies

### Depends on

- [mdd-form-input](../form-input)

### Graph
```mermaid
graph TD;
  mdd-form-select-filter --> mdd-form-input
  style mdd-form-select-filter fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
