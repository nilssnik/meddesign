import { Component, h } from '@stencil/core';

@Component({
  tag: 'med-design',
  styleUrl: 'med-design.scss',
})
export class MEDDesign {
  handleKeyDown(e: KeyboardEvent) {
    if (e.key === 'Tab') {
      document.body.classList.remove('using-mouse');
    }
  }

  handleMouseDown() {
    document.body.classList.add('using-mouse');
  }

  connectedCallback() {
    window.addEventListener('keydown', this.handleKeyDown);
    window.addEventListener('mousedown', this.handleMouseDown);
  }

  disconnectedCallback() {
    window.removeEventListener('keydown', this.handleKeyDown);
    window.removeEventListener('mousedown', this.handleMouseDown);
  }

  render() {
    return <slot />;
  }
}
